import pytest
from bessyii.plans.flying import mov_count, rel_mov_count

from ophyd.sim import motor,det, noisy_det

from databroker.v2 import temp
from bluesky.run_engine import RunEngineInterrupted


from bluesky.callbacks.best_effort import BestEffortCallback
from event_model import RunRouter
from bluesky import RunEngine

RE = RunEngine({})
db = temp()
RE.subscribe(db.v1.insert)



from bluesky.preprocessors import SupplementalData
sd = SupplementalData()
RE.preprocessors.append(sd)

#test things work with the baseline
from ophyd.sim import noisy_det
sd.baseline = [noisy_det]

import threading

def test_mov_count():

    motor.delay = 1
    initial_velocity = 0.2
    scan_velocity = 0.5

    #Set initial velocity
    motor.velocity.set(initial_velocity)
    RE(mov_count([det],motor,1,2,scan_velocity))
    
    run = db[-1]
    data = run.primary.read()
    config = run.primary.config['motor'].read()

    # check that the velocity is changed during the scan
    assert scan_velocity == config['motor_velocity'].values[0]

    # check that it's set back
    assert motor.velocity.get() == initial_velocity

    # Check that the data contains both the motor and the detector
    assert 'motor' in data
    assert 'det' in data

def test_mov_count_abort_reset():
    # model a ctrl+c then abort event, which should set the velocity back to initial

    motor.delay = 4
    initial_velocity = 0.2
    scan_velocity = 0.5
    
    #Set initial velocity
    motor.velocity.set(initial_velocity)

    # Use threading to issue these commands some time in the future
    threading.Timer(motor.delay +3, RE.request_pause, (True,)).start()
    threading.Timer(motor.delay +3.5, RE.abort, (True,)).start()
    with pytest.raises(RunEngineInterrupted):
        RE(mov_count([det],motor,1,2,scan_velocity))
    
    run = db[-1]
    config = run.primary.config['motor'].read()

    # Check we mark the exit status as aborted
    assert run.metadata['stop']['exit_status']== 'abort'

    # check that the velocity is changed during the scan
    assert scan_velocity == config['motor_velocity'].values[0]

    # check that it's set back even after the exception is raised
    assert motor.velocity.get() == initial_velocity

def test_rel_mov_count():

    motor.delay = 1
    initial_velocity = 0.2
    scan_velocity = 0.5

    initial_pos = motor.position
    distance = 2
    #Set initial velocity
    motor.velocity.set(initial_velocity)
    RE(rel_mov_count([det],motor,distance,scan_velocity))
    
    run = db[-1]
    data = run.primary.read()
    config = run.primary.config['motor'].read()

    # check that the velocity is changed during the scan
    assert scan_velocity == config['motor_velocity'].values[0]

    # check that it's set back
    assert motor.velocity.get() == initial_velocity

    # Check that the data contains both the motor and the detector
    assert 'motor' in data
    assert 'det' in data

    # Check that the move was relative

    assert data['motor'].values[0] == initial_pos
    assert data['motor'].values[-1] - initial_pos - distance <= 0.2

def test_rel_mov_count_abort_reset():
    # model a ctrl+c then abort event, which should set the velocity back to initial

    motor.delay = 4
    initial_velocity = 0.2
    scan_velocity = 0.5
    
    #Set initial velocity
    motor.velocity.set(initial_velocity)

    # Use threading to issue these commands some time in the future
    threading.Timer(1, RE.request_pause, (True,)).start()
    threading.Timer(1.5, RE.abort, (True,)).start()
    with pytest.raises(RunEngineInterrupted):
        RE(rel_mov_count([det],motor,2,scan_velocity))
    
    run = db[-1]
    config = run.primary.config['motor'].read()

    # Check we mark the exit status as aborted
    assert run.metadata['stop']['exit_status']== 'abort'

    # check that the velocity is changed during the scan
    assert scan_velocity == config['motor_velocity'].values[0]

    # check that it's set back even after the exception is raised
    assert motor.velocity.get() == initial_velocity