import pytest
import bessyii_devices
from ophyd.sim import motor, noisy_det, det2
from bluesky.plans import count
from bluesky.preprocessors import SupplementalData
from bluesky import RunEngine
from databroker.v2 import temp
import icat 
from icat.query import Query
from bessyii.eLog import requestInvestigationName, getSessionID, authenticate_session

## Set up env
RE = RunEngine({})
db = temp()
RE.subscribe(db.v1.insert)


# Create a baseline to test that
sd = SupplementalData()

sd.baseline = [noisy_det] # later for uthenticate_session?
RE.preprocessors.append(sd)

def session_check():
    session_id, full_name = getSessionID('nbour','pwnbour',"https://0.0.0.0:443")
    if session_id!= None and full_name!=None:
        return True
    else:
        return False

def test_icat_login():
    assert session_check()
 