def add_to_global_scope(objects: dict, global_scope: dict):
    """
    Add key-value pairs from a dictionary to the global scope as variables.

    This function takes a dictionary where the keys are intended to be the names
    of global variables and the values are the corresponding objects. It adds
    these key-value pairs to the global scope dictionary provided by `global_scope`.
    If a variable name already exists in the global scope, a ValueError is raised
    to prevent overwriting.

    Parameters
    ----------
    objects : dict
        A dictionary where each key is the intended name of a global variable and
        each value is the object to be assigned to that global variable name.

    global_scope : dict
        The dictionary representing the global scope where the variables should
        be added. Typically, this would be the dictionary returned by `globals()`.

    Raises
    ------
    ValueError
        If a key from the `objects` dictionary already exists as a key in the
        `global_scope` dictionary, a ValueError is raised to prevent overwriting
        existing global variables.

    Examples
    --------
        devices = {
            "device1": "instance1",
            "device2": "instance2",
        }
        add_to_global_scope(devices, globals())
        print(device1)  # Output: instance1
        print(device2)  # Output: instance2
    """

    for obj_name, obj_instance in objects.items():
        # Before adding a new object to the scope of global variables assert that it is does not already exist.
        # We do not want to overwrite already existing objects in the global scope

        if obj_name not in global_scope:
            global_scope[obj_name] = obj_instance
        else:
            raise ValueError(
                f"Add to the global scope an object with the name: {obj_name} FAILED. Object with the name: {obj_name} already exists in the scope of global variables."
            )
